# Introduction

This repository contains the code that's referenced by the COM S 513 final project report. In this repositroy exists all tools used, all test programs used, and all output artifacts analyzed.

# Directory roles

- `testcases`: Holds the source and compiled code for each fuzzer. For example, `testcases/gzip/gzip-1.2.4-afl` holds the compiled gzip code with AFL++ instrumentation. `testcases/gzip/gzip-1.2.4-afl` holds the compiled gzip code with Honggfuzz instrumentation. Also, each fuzzers outputs are housed in their respective directories for each program. For example, `testcases/gzip/afl` holds the outputs generated when fuzzing fzip with AFL++. `testcases/gzip/hfuzz` olds the outputs generated when fuzzing fzip with Honggfuzz.
- `tools`: This directory holds the compiled code or environment for each fuzzer used. For example, `tools/afl` holds the compiled code for AFL++; when performing fuzzing using this program, run `./tools/afl/afl-fuzz` from the project root with the proper runtime arguments. `tools/honggfuzz` holds the compiled code for Honggfuzz as well as other runtime artifacts; when performing fuzzing using this program, run `./tools/hongfuzz/honggfuzz` from the project root with the proper runtime arguments.

# Use of repository tools
Below is a collection of all commands used to generate fuzzing outputs. Each command was run from the tool directory (`tools/afl` for AFL++, `tools/honggfuzz` for Honggfuzz).

## get_sign

### AFL++

Command to compile get_sign with AFL++ instrumentation. Ran within the `testcases/get_sign` directory.
```bash
../../tools/afl/afl-clang-fast ../../testcases/get_sign/get_sign.c -o ../../testcases/get_sign/get_sign_afl
```

Command to run get_sign with AFL++ fuzzing. Ran within the `tools/afl` directory
```bash
./afl-fuzz -i ../../testcases/get_sign/fuzzin -o ../../testcases/get_sign/afl -D -m 1000 \
-- ../../testcases/get_sign/get_sign_afl
```

### Honggfuzz

Command to compile get_sign with Honggfuzz instrumentation. Ran within the `testcases/get_sign` directory.
```bash
../../tools/honggfuzz/hfuzz_cc/hfuzz-clang ../../testcases/get_sign/get_sign.c -o ../../testcases/get_sign/get_sign_hfuzz
```

Command to run get_sign with Honggfuzz fuzzing. Ran within the `tools/honggfuzz` directory
```bash
./honggfuzz -i ../../testcases/get_sign/fuzzin -o ../../testcases/get_sign/hfuzz/out -W ../../testcases/get_sign/hfuzz/workspace \
--covdir_new ../../testcases/get_sign/hfuzz/coverage --crashdir ../../testcases/get_sign/hfuzz/crash -n 4 -s -V \
-- ../../testcases/get_sign/get_sign_hfuzz
```

## gzip

### AFL++

Command to compile gzip with AFL instrumentation. Ran within the `testcases/gzip/gzip-1.2.4-afl` directory.
```bash
CC=../../tools/afl/afl-clang-fast CXX=../../tools/afl/afl-clang-fast++ \
./configure --disabled-shared \
&& make
```

Command to run gzip with AFL fuzzing. Ran within the `tools/afl` directory.
```bash
./afl-fuzz -i ../../testcases/gzip/fuzzin -o ../../testcases/gzip/afl -D -m 1000 \
-- ../../testcases/gzip/gzip-1.2.4-afl/gzip
```

### Honggfuzz

Code to compile gzip with Hongfuzz instrumtation. Ran within the `testcases/gzip/gzip-1.2.4-hfuzz` directory.
```bash
CC=../../tools/honggfuzz/hfuzz_cc/hfuzz-clang CXX=../../tools/honggfuzz/hfuzz_cc/hfuzz-clang++ \
./configure --disabled-shared \
&& make
```

Command to run gzip with Honggfuzz fuzzing. Ran within the `tools/honggfuzz` directory.
```bash
./honggfuzz -i ../../testcases/gzip/fuzzin -o ../../testcases/gzip/hfuzz/out -W ../../testcases/gzip/hfuzz/workspace \
--covdir_new ../../testcases/gzip/hfuzz/coverage --crashdir ../../testcases/gzip/hfuzz/crash -n 4 -s -V \
-- ../../testcases/gzip/gzip-1.2.4-hfuzz/gzip
```

## OpenSSL
### AFL++

Command to compile OpenSSL with AFL++ instrumtation. Ran within the `testcases/openssl/openssl-1.0.1f-afl` project directory.

Note: `afl-clang-fast` and `afl-clang-fast++` need to exist in the `/usr/bin` directory. This program will take ~15-30 minutes to compile depending on machine specs
```bash
CC=../../tools/afl/afl-clang-fast CXX=../../tools/afl/afl-clang-fast++ ./Configure linux-x86_64 no-shared zlib enable-tlsext \
--cross-compile-prefix=/usr/bin \
&& make
```

Command to run OpenSSL with AFL fuzzing. Ran within the `tools/afl` directory.
```bash
./afl-fuzz -i ../../testcases/openssl/fuzzin -o ../../testcases/openssl/afl -D -m 1000 \
-- ../../testcases/openssl/openssl-1.0.1f-afl/apps/openssl
```

### Honggfuzz

Command to compile OpenSSL with Honggfuzz instrumtation. Ran within the `testcases/openssl/openssl-1.0.1f-hfuzz` project directory.

Note: `hfuzz-clang` and `hfuzz-clang++` need to exist in the `/usr/bin` directory. This program will take ~15-30 minutes to compile depending on machine specs
```bash
CC=../../tools/honggfuzz/hfuzz_cc/hfuzz-clang CXX=../../tools/honggfuzz/hfuzz_cc/hfuzz-clang++ ./Configure linux-x86_64 no-shared zlib enable-tlsext \
--cross-compile-prefix=/usr/bin \
&& make
```

Command to run OpenSSL with Honggfuzz fuzzing. Ran within the `tools/honggfuzz` directory.
```bash
./honggfuzz -i ../../testcases/openssl/fuzzin -o ../../testcases/openssl/hfuzz/out -W ../../testcases/openssl/hfuzz/workspace \
--covdir_new ../../testcases/openssl/hfuzz/coverage --crashdir ../../testcases/openssl/hfuzz/crash -n 4 -s -V \
-- ../../testcases/openssl/openssl-1.0.1f-hfuzz/apps/openssl
```

# Contact
For more information/assistance contact Sean Griffen at griffens@iastate.edu