/*
 * First KLEE tutorial: testing a small function
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define SIZE 10

int get_sign(int x) {
  if (x == 0)  return 0;
  if (x < 0)   return -1;
  else         return 1;
} 

int main(int argc, char** argv) {
   char s[SIZE];

   printf("Enter number to get sign of: ");
   gets(s);
   
   int sign = get_sign(atoi(s));
   if (sign == 0) { printf("Number is zero\n"); }
   else if (sign < 0) { printf("Number is negative\n"); }
   else { printf("Number is positive\n"); }

   return 0;
}